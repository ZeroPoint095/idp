import nxppy
import time
import RPi.GPIO as GPIO
#led colors
g = 40 #pin rechts onder
y = 38 #pin 2e van onder, rechts
r = 36 #pin 3e van onder, rechts
buzz = 37
GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(g, GPIO.OUT)
GPIO.setup(r, GPIO.OUT)
GPIO.setup(y, GPIO.OUT)
GPIO.setup(buzz, GPIO.OUT)
mifare = nxppy.Mifare()
card={'04D9306A643480':'Kaart nummer 1','0499306A643480':'Kaart nummer 2', '01020304':'Telefoon nummer 1'}

try:
    # here you put your main loop or block of code
    def return_uid():
        "Attempts to scan card UID once per second, for 5 seconds"
        while True:
            try:
                uid = mifare.select()
                x = 0
                while x > 5:
                    GPIO.output(y, False)
                    time.sleep(1)
                    GPIO.output(y, True)        #flashed LED when card is read
                    counter =+1
                print(('UID: '+uid))
                return uid
            except nxppy.SelectError:
                #selecterror is raised if no card is in the field
                GPIO.output(y, True)
                #print('No card')
                pass
            time.sleep(1)

    def poortOpen(status):
        "Toegang wordt verleend obv string 'ingecheck', 'uitgecheckt', of [else]"
        if status == 'uitgecheckt':
            #toegang verlenen
            GPIO.output(r, False)
            GPIO.output(y, False)
            GPIO.output(g, True)
            #print((card[uid]))
            print('Access granted\n')
            GPIO.output(buzz, True)
            time.sleep(0.1)
            GPIO.output(buzz, False)
            time.sleep(0.1)
            GPIO.output(buzz, True)
            time.sleep(0.1)
            GPIO.output(buzz, False)
            time.sleep(3)
            GPIO.output(g, False)
        elif status =='ingecheckt':
            #gebruikter uitchecken
            GPIO.output(r, False)
            GPIO.output(y, False)
            GPIO.output(g, True)
            #print((card[uid]))
            print('Checked Out\n')
            GPIO.output(buzz, True)
            time.sleep(0.4)
            GPIO.output(buzz, False)
            time.sleep(3)
            GPIO.output(g, False)
        else:#geen toegang
            GPIO.output(g, False)
            GPIO.output(y, False)
            GPIO.output(r, True)
            print('Onbekend kaart\nAccess denied\n')
            GPIO.output(buzz, True)
            time.sleep(0.5)
            GPIO.output(buzz, False)
            time.sleep(0.5)
            GPIO.output(buzz, True)
            time.sleep(0.5)
            GPIO.output(buzz, False)
            time.sleep(0.5)
            GPIO.output(buzz, True)
            time.sleep(0.5)
            GPIO.output(buzz, False)
            time.sleep(3)
            time.sleep(1)
            GPIO.output(r, False)

    def test():
        "als test ipv database"
        #dict met 2 NXP nfc kaarten en android van Q

        while True:
            uid = return_uid()
            if uid in card:
                poortOpen('ingecheckt')
            else:
                poortOpen('geen toegang')
    test()
    #poortOpen('ingecheckt')

except KeyboardInterrupt:
    # here you put any code you want to run before the program
    # exits when you press CTRL+C
    print('Keyboard Interrupt')

finally:
    GPIO.cleanup() # this ensures a clean exit
